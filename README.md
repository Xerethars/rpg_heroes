# RPGHeroes

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

C# .NET project delivery for the assignment 1 module at Noroff. It reproduces the functionality requested in the assignment 1 file, as well as testing the produced functionality with XUnit.

The main domain of the program is the Hero class. A Hero can be a Rogue, Ranger, Warrior or a Mage. The user is then able to provide a name for their hero. Each hero class has a different main attribute and different scaling with gained level. For example a Mage is scaling up Intelligence for each level gained with their secondary stats (strength and dexterity) increasing at a slower pace.

Each hero is able to equip a weapon as well as armor by storing each HeroEquipment subclass item in the corresponding hero's Equipment dictionary. The equipment itself contributes to a single hero's damage by either providing weapon damage scaling in the weapon slot, or by providing an increase to attributes from armor in the remaining equipment slots. The damage itself is calculated realtime.

In order to equip an equipment piece the hero has to meet two requirements. First, a hero must have their level at least equal to a single equipments required level property. Second, a hero has to be able to equip the type of equipment they try to equip. For instance, a warrior can only equip armor of type Plate and Mail. If a hero tries to equip any equipment which they cannot wear, a corresponding exception is thrown.

Lastly, the RPGHeroes project is supplemented with Enums responsible for keeping track of different equipment. Additionally, the entire project is being thoroughly tested with the help of the XUnit framework. In order to ensure requested functionality is being met and sustained. 

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [License](#license)

## Background
The background for this project was a mandatory assignment at Noroff Accelerate.

## Install
To install the project, simply clone the repository and open it up in Visual Studio.

#### Dependencies
.NET6.0
XUnit 2.4.2

## Usage
In order to run the project, open up the project folder in Visual Studio and run all the tests found in the .Test folder.

## Maintainers

[@Xerethars](https://github.com/Xerethars)

## License

MIT © 2022 Michal