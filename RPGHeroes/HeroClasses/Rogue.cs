﻿using RPGHeroes.HeroAddons;
using RPGHeroes.HeroAddons.Enum;

namespace RPGHeroes.HeroClasses
{
    public class Rogue : HeroClass
    {
        public Rogue(string name) : base(name)
        {
            LevelAttribute = new HeroAttributes(2, 6, 1);
            ValidWeaponTypes = new WeaponTypes[] { WeaponTypes.Dagger, WeaponTypes.Sword };
            ValidArmorTypes = new ArmorTypes[] { ArmorTypes.Mail, ArmorTypes.Leather };
        }

        public override double Damage()
        {
            Weapon? wep = (Weapon?)Equipment[EquipmentSlots.Weapon];
            int weaponDamage = wep != null ? wep.WeaponDamage : 1;

            return weaponDamage * (1 + (TotalAttributes().Dexterity / 100));
        }

        public override void LevelUp()
        {
            base.LevelUp();
            LevelAttribute.AddAttributes(1, 4, 1);
        }
    }
}
