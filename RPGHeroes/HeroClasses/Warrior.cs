﻿using RPGHeroes.HeroAddons;
using RPGHeroes.HeroAddons.Enum;

namespace RPGHeroes.HeroClasses
{
    public class Warrior : HeroClass
    {
        public Warrior(string name) : base(name)
        {
            LevelAttribute = new HeroAttributes(5, 2, 1);
            ValidWeaponTypes = new WeaponTypes[] { WeaponTypes.Axe, WeaponTypes.Hammer, WeaponTypes.Sword };
            ValidArmorTypes = new ArmorTypes[] { ArmorTypes.Plate, ArmorTypes.Mail };
        }

        public override double Damage()
        {
            Weapon? wep = (Weapon?)Equipment[EquipmentSlots.Weapon];
            int weaponDamage = wep != null ? wep.WeaponDamage : 1;

            return weaponDamage * (1 + (TotalAttributes().Strength / 100));
        }

        public override void LevelUp()
        {
            base.LevelUp();
            LevelAttribute.AddAttributes(3, 2, 1);
        }
    }
}
