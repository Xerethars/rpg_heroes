﻿using RPGHeroes.Exceptions;
using RPGHeroes.HeroAddons;
using RPGHeroes.HeroAddons.Enum;
using System.Text;

namespace RPGHeroes.HeroClasses
{
    public abstract class HeroClass
    {

        public string Name { get; init; }
        public int Level { get; set; }
        public HeroAttributes LevelAttribute { get; set; }
        public Dictionary<EquipmentSlots, HeroEquipment?> Equipment { get; set; }
        public WeaponTypes[] ValidWeaponTypes { get; init; }
        public ArmorTypes[] ValidArmorTypes { get; init; }

        public HeroClass(string name)
        {
            Name = name;
            Level = 1;
            Equipment = new Dictionary<EquipmentSlots, HeroEquipment?>();
            Equipment.Add(EquipmentSlots.Weapon, null);
            Equipment.Add(EquipmentSlots.Head, null);
            Equipment.Add(EquipmentSlots.Body, null);
            Equipment.Add(EquipmentSlots.Legs, null);
        }
        public HeroAttributes TotalAttributes()
        {
            int totalStr = LevelAttribute.Strength, 
                totalDex = LevelAttribute.Dexterity, totalInt = LevelAttribute.Intelligence;

            foreach(HeroEquipment item in Equipment.Values)
            {
                if (item is Armor armoritem)
                {
                    totalStr += armoritem.ArmorAttributes.Strength;
                    totalDex += armoritem.ArmorAttributes.Dexterity;
                    totalInt += armoritem.ArmorAttributes.Intelligence;
                }
            }

            return new HeroAttributes(totalStr, totalDex, totalInt);
        }
        public void EquipWeapon(Weapon weapon)
        {
            if(!CanEquipWeapon(weapon))
            {
                throw new InvalidWeaponException("Your class can't use this weapontype or your level is too low");
            }

            Equipment[weapon.Slot] = weapon;
        }

        private bool CanEquipWeapon(Weapon weapon)
        {
            return ValidWeaponTypes.Contains(weapon.WeaponType) && weapon.RequiredLevel <= Level;
        }
        public void EquipArmor(Armor armor)
        {
            if(!CanEquipArmor(armor))
            {
                throw new InvalidArmorException("Your class can't use this armortype or your level is too low");
            }

            Equipment[armor.Slot] = armor;
        }

        private bool CanEquipArmor(Armor armor)
        {
            return ValidArmorTypes.Contains(armor.ArmorType) && armor.RequiredLevel <= Level;
        }
        public virtual void LevelUp()
        {
            Level += 1;
        }
        public string Display()
        {
            StringBuilder builder = new StringBuilder();
            HeroAttributes totalAttributes = TotalAttributes();

            builder.AppendLine("Name: " + Name);
            builder.AppendLine("Class: " + this.GetType().Name);
            builder.AppendLine("Level: " + Level);
            builder.AppendLine("Total Strength: " + totalAttributes.Strength);
            builder.AppendLine("Total Dexterity: " + totalAttributes.Dexterity);
            builder.AppendLine("Total Intelligence: " + totalAttributes.Intelligence);
            builder.AppendLine("Damage: " + Damage());

            return builder.ToString();
        }

        public abstract double Damage();
    }
}
