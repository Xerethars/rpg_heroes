﻿using RPGHeroes.HeroAddons;
using RPGHeroes.HeroAddons.Enum;
using System.Net;

namespace RPGHeroes.HeroClasses
{
    public class Mage : HeroClass
    {
        public Mage(string name) : base(name)
        {
            LevelAttribute = new HeroAttributes(1, 1, 8);
            ValidWeaponTypes = new WeaponTypes[] { WeaponTypes.Staff, WeaponTypes.Wand };
            ValidArmorTypes = new ArmorTypes[] { ArmorTypes.Cloth };
        }

        public override double Damage()
        {
            Weapon? wep = (Weapon?)Equipment[EquipmentSlots.Weapon];
            int weaponDamage = wep != null ? wep.WeaponDamage : 1;

            return weaponDamage * (1 + (TotalAttributes().Intelligence / 100));
        }

        public override void LevelUp()
        {
            base.LevelUp();
            LevelAttribute.AddAttributes(1, 1, 5);
        }

    }
}
