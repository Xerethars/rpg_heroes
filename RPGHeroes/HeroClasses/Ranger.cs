﻿using RPGHeroes.HeroAddons;
using RPGHeroes.HeroAddons.Enum;

namespace RPGHeroes.HeroClasses
{
    public class Ranger : HeroClass
    {
        public Ranger(string name) : base(name)
        {
            LevelAttribute = new HeroAttributes(1, 7, 1);
            ValidWeaponTypes = new WeaponTypes[] { WeaponTypes.Bow };
            ValidArmorTypes = new ArmorTypes[] { ArmorTypes.Leather, ArmorTypes.Mail };
        }

        public override double Damage()
        {
            Weapon? wep = (Weapon?)Equipment[EquipmentSlots.Weapon];
            int weaponDamage = wep != null ? wep.WeaponDamage : 1;

            return weaponDamage * (1 + (TotalAttributes().Dexterity / 100));
        }

        public override void LevelUp()
        {
            base.LevelUp();
            LevelAttribute.AddAttributes(1, 5, 1);
        }
    }
}
