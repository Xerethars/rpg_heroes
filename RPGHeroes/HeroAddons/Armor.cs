﻿using RPGHeroes.HeroAddons.Enum;

namespace RPGHeroes.HeroAddons
{
    public class Armor : HeroEquipment
    {
        public ArmorTypes ArmorType { get; init; }
        public HeroAttributes ArmorAttributes { get; init; }

        public Armor(string name, int requiredLevel, EquipmentSlots slot
            , ArmorTypes armorType, HeroAttributes armorAttributes)
            : base(name, requiredLevel, slot)
        {
            ArmorType = armorType;
            ArmorAttributes = armorAttributes;
        }
    }
}
