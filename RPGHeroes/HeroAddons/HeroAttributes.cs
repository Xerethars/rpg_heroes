﻿namespace RPGHeroes.HeroAddons
{
    public class HeroAttributes
    {
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }

        public HeroAttributes(int startStr, int startDex, int startInt)
        {
            Strength = startStr;
            Dexterity = startDex;
            Intelligence = startInt;
        }
        public void AddAttributes(int strength, int dexterity, int intelligence)
        {
            Strength += strength;
            Dexterity += dexterity;
            Intelligence += intelligence;
        }
    }
}
