﻿using RPGHeroes.HeroAddons.Enum;

namespace RPGHeroes.HeroAddons
{
    public class Weapon : HeroEquipment
    {
        public WeaponTypes WeaponType { get; set; }
        public int WeaponDamage { get; set; }

        public Weapon(string name, int requiredLevel, WeaponTypes weaponType, int damage)
            : base(name, requiredLevel, EquipmentSlots.Weapon)
        {
            WeaponType = weaponType;
            WeaponDamage = damage;
        }

    }
}
