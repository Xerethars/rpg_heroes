﻿namespace RPGHeroes.HeroAddons.Enum
{
    public enum EquipmentSlots
    {
        Weapon,
        Head,
        Body,
        Legs
    }
}
