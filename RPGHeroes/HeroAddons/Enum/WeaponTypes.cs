﻿namespace RPGHeroes.HeroAddons.Enum
{
    public enum WeaponTypes
    {
        Axe,
        Bow,
        Dagger,
        Hammer,
        Staff,
        Sword,
        Wand
    }
}
