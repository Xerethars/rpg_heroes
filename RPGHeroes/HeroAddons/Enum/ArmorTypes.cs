﻿namespace RPGHeroes.HeroAddons.Enum
{
    public enum ArmorTypes
    {
        Cloth,
        Leather,
        Mail,
        Plate
    }
}
