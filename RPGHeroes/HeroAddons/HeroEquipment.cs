﻿using RPGHeroes.HeroAddons.Enum;

namespace RPGHeroes.HeroAddons
{
    public abstract class HeroEquipment
    {
        public string Name { get; init; }
        public int RequiredLevel { get; init; }
        public EquipmentSlots Slot { get; init; }

        public HeroEquipment(string name, int requiredLevel, EquipmentSlots slot)
        {
            Name = name;
            RequiredLevel = requiredLevel;
            Slot = slot;
        }
    }
}
