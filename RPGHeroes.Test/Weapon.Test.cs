﻿using RPGHeroes.HeroAddons;
using RPGHeroes.HeroAddons.Enum;

namespace RPGHeroes.Test
{
    public class WeaponTest
    {
        [Fact]
        public void Constructor_WeaponParams_CreatesWeaponWithCorrctName()
        {
            Weapon weapon = new Weapon("Phantom Axe", 1, WeaponTypes.Axe, 1);
            string expected = "Phantom Axe";

            string actual = weapon.Name;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Contructor_WeaponParams_CreatesWeaponWithCorrectRequiredLevel()
        {
            Weapon weapon = new Weapon("Phantom Axe", 1, WeaponTypes.Axe, 1);
            int expected = 1;

            int actual = weapon.RequiredLevel;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Contructor_WeaponParams_CreatesWeaponWithCorrectSlot()
        {
            Weapon weapon = new Weapon("Phantom Axe", 1, WeaponTypes.Axe, 1);
            EquipmentSlots expected = EquipmentSlots.Weapon;

            EquipmentSlots actual = weapon.Slot;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Contructor_WeaponParams_CreatesWeaponWithCorrectWeaponType()
        {
            Weapon weapon = new Weapon("Phantom Axe", 1, WeaponTypes.Axe, 1);
            WeaponTypes expected = WeaponTypes.Axe;

            WeaponTypes actual = weapon.WeaponType;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Contructor_WeaponParams_CreatesWeaponWithCorrectDamage()
        {
            Weapon weapon = new Weapon("Phantom Axe", 1, WeaponTypes.Axe, 1);
            int expected = 1;

            int actual = weapon.WeaponDamage;

            Assert.Equal(expected, actual);
        }
    }
}
