﻿using RPGHeroes.HeroAddons;
using RPGHeroes.HeroClasses;

namespace RPGHeroes.Test
{
    public class WarriorTest
    {
        [Fact]
        public void Constructor_HeroName_CreatesWarriorWithCorrectName()
        {
            Warrior warrior = new Warrior("Frank");
            string expected = "Frank";

            string actual = warrior.Name;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Constructor_HeroName_CreatesWarriorWithCorrectStartingLevel()
        {
            Warrior warrior = new Warrior("Frank");
            int expected = 1;

            int actual = warrior.Level;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Constructor_HeroName_CreatesWarriorWithCorrectStartingAttributes()
        {
            Warrior warrior = new Warrior("Frank");
            int expectedStrength = 5;
            int expectedDexterity = 2;
            int expectedIntelligence = 1;

            Assert.Multiple(
                () => Assert.Equal(expectedStrength, warrior.LevelAttribute.Strength),
                () => Assert.Equal(expectedDexterity, warrior.LevelAttribute.Dexterity),
                () => Assert.Equal(expectedIntelligence, warrior.LevelAttribute.Intelligence)
            );
        }

        [Fact]
        public void LevelUp_SingleLevel_IncreasesLevelByOne()
        {
            Warrior warrior = new Warrior("Frank");
            int expected = 2;

            warrior.LevelUp();
            int actual = warrior.Level;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_SingleLevel_IncreaseAttributesByStr1Dex5Int1()
        {
            Warrior warrior = new Warrior("Frank");
            int expectedStrength = warrior.LevelAttribute.Strength + 3;
            int expectedDexterity = warrior.LevelAttribute.Dexterity + 2;
            int expectedIntelligence = warrior.LevelAttribute.Intelligence + 1;

            warrior.LevelUp();

            Assert.Multiple(
                () => Assert.Equal(expectedStrength, warrior.LevelAttribute.Strength),
                () => Assert.Equal(expectedDexterity, warrior.LevelAttribute.Dexterity),
                () => Assert.Equal(expectedIntelligence, warrior.LevelAttribute.Intelligence)
            );
        }

        [Fact]
        public void Damage_NoWeapon_ReturnsCorrectDamage()
        {
            Warrior warrior = new Warrior("Frank");
            int weaponDamage = 1;
            int scalingFactor = 1;
            int damageAttribute = warrior.TotalAttributes().Strength;
            double expected = weaponDamage * (scalingFactor + damageAttribute / 100);

            double actual = warrior.Damage();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Damage_EquippedWeapon_ReturnsCorrectDamage()
        {
            Warrior warrior = new Warrior("Frank");
            int weaponDamage = 2;
            int scalingFactor = 1;
            int damageAttribute = warrior.TotalAttributes().Strength;
            Weapon weapon = new Weapon("Gigachad Hammer", 1,
                HeroAddons.Enum.WeaponTypes.Hammer, weaponDamage);
            double expected = weapon.WeaponDamage * (scalingFactor + damageAttribute / 100);

            warrior.EquipWeapon(weapon);
            double actual = warrior.Damage();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Damage_ReplacedWeapon_ReturnsCorrectDamage()
        {
            Warrior warrior = new Warrior("Frank");
            int weaponDamage = 2;
            int scalingFactor = 1;
            int damageAttribute = warrior.TotalAttributes().Strength;

            Weapon weapon = new Weapon("Gigachad Hammer", 1,
                HeroAddons.Enum.WeaponTypes.Hammer, weaponDamage);
            Weapon replacingWeapon = new Weapon("Better Hammer", 1,
                HeroAddons.Enum.WeaponTypes.Hammer, weaponDamage * 2);

            double expected = replacingWeapon.WeaponDamage * (scalingFactor + damageAttribute / 100);

            warrior.EquipWeapon(weapon);
            warrior.EquipWeapon(replacingWeapon);
            double actual = warrior.Damage();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Damage_WeaponAndArmor_ReturnsCorrectDamage()
        {
            Warrior warrior = new Warrior("Frank");
            int weaponDamage = 1;
            int scalingFactor = 1;

            Weapon weapon = new Weapon("Gigachad Hammer", 1,
                HeroAddons.Enum.WeaponTypes.Hammer, weaponDamage);
            Armor armor = new Armor("Gigachad Plate", 1, HeroAddons.Enum.EquipmentSlots.Body,
                HeroAddons.Enum.ArmorTypes.Plate, new HeroAttributes(1, 1, 1));

            int damageAttribute = warrior.TotalAttributes().Strength + armor.ArmorAttributes.Strength;
            double expected = weapon.WeaponDamage * (scalingFactor + damageAttribute / 100);

            warrior.EquipWeapon(weapon);
            warrior.EquipArmor(armor);
            double actual = warrior.Damage();

            Assert.Equal(expected, actual);
        }
    }
}
