﻿using RPGHeroes.HeroAddons;
using RPGHeroes.HeroClasses;

namespace RPGHeroes.Test
{
    public class MageTest
    {
        [Fact]
        public void Constructor_HeroName_CreatesMageWithCorrectName()
        {
            Mage mage = new Mage("Frank");
            string expected = "Frank";

            string actual = mage.Name;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Constructor_HeroName_CreatesMageWithCorrectStartingLevel()
        {
            Mage mage = new Mage("Frank");
            int expected = 1;

            int actual = mage.Level;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Constructor_HeroName_CreatesMageWithCorrectStartingAttributes()
        {
            Mage mage = new Mage("Frank");
            int expectedStrength = 1;
            int expectedDexterity = 1;
            int expectedIntelligence = 8;

            Assert.Multiple(
                () => Assert.Equal(expectedStrength, mage.LevelAttribute.Strength),
                () => Assert.Equal(expectedDexterity, mage.LevelAttribute.Dexterity),
                () => Assert.Equal(expectedIntelligence, mage.LevelAttribute.Intelligence)
            );
        }

        [Fact]
        public void LevelUp_SingleLevel_IncreasesLevelByOne()
        {
            Mage mage = new Mage("Frank");
            int expected = 2;

            mage.LevelUp();
            int actual = mage.Level;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_SingleLevel_IncreaseAttributesByStr1Dex5Int1()
        {
            Mage mage = new Mage("Frank");
            int expectedStrength = mage.LevelAttribute.Strength + 1;
            int expectedDexterity = mage.LevelAttribute.Dexterity + 1;
            int expectedIntelligence = mage.LevelAttribute.Intelligence + 5;

            mage.LevelUp();

            Assert.Multiple(
                () => Assert.Equal(expectedStrength, mage.LevelAttribute.Strength),
                () => Assert.Equal(expectedDexterity, mage.LevelAttribute.Dexterity),
                () => Assert.Equal(expectedIntelligence, mage.LevelAttribute.Intelligence)
            );
        }

        [Fact]
        public void Damage_NoWeapon_ReturnsCorrectDamage()
        {
            Mage mage = new Mage("Frank");
            int weaponDamage = 1;
            int scalingFactor = 1;
            int damageAttribute = mage.TotalAttributes().Intelligence;
            double expected = weaponDamage * (scalingFactor + damageAttribute / 100);

            double actual = mage.Damage();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Damage_EquippedWeapon_ReturnsCorrectDamage()
        {
            Mage mage = new Mage("Frank");
            int weaponDamage = 2;
            int scalingFactor = 1;
            int damageAttribute = mage.TotalAttributes().Intelligence;
            Weapon weapon = new Weapon("Gigachad Wand", 1,
                HeroAddons.Enum.WeaponTypes.Wand, weaponDamage);
            double expected = weapon.WeaponDamage * (scalingFactor + damageAttribute / 100);

            mage.EquipWeapon(weapon);
            double actual = mage.Damage();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Damage_ReplacedWeapon_ReturnsCorrectDamage()
        {
            Mage mage = new Mage("Frank");
            int weaponDamage = 2;
            int scalingFactor = 1;
            int damageAttribute = mage.TotalAttributes().Intelligence;

            Weapon weapon = new Weapon("Gigachad Wand", 1,
                HeroAddons.Enum.WeaponTypes.Wand, weaponDamage);
            Weapon replacingWeapon = new Weapon("Better Wand", 1,
                HeroAddons.Enum.WeaponTypes.Wand, weaponDamage * 2);

            double expected = replacingWeapon.WeaponDamage * (scalingFactor + damageAttribute / 100);

            mage.EquipWeapon(weapon);
            mage.EquipWeapon(replacingWeapon);
            double actual = mage.Damage();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Damage_WeaponAndArmor_ReturnsCorrectDamage()
        {
            Mage mage = new Mage("Frank");
            int weaponDamage = 1;
            int scalingFactor = 1;

            Weapon weapon = new Weapon("Gigachad Wand", 1,
                HeroAddons.Enum.WeaponTypes.Wand, weaponDamage);
            Armor armor = new Armor("Gigachad Cloth", 1, HeroAddons.Enum.EquipmentSlots.Body,
                HeroAddons.Enum.ArmorTypes.Cloth, new HeroAttributes(1, 1, 1));

            int damageAttribute = mage.TotalAttributes().Intelligence + armor.ArmorAttributes.Intelligence;
            double expected = weapon.WeaponDamage * (scalingFactor + damageAttribute / 100);

            mage.EquipWeapon(weapon);
            mage.EquipArmor(armor);
            double actual = mage.Damage();

            Assert.Equal(expected, actual);
        }
    }
}
