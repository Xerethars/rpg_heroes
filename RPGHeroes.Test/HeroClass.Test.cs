﻿using RPGHeroes.Exceptions;
using RPGHeroes.HeroAddons;
using RPGHeroes.HeroAddons.Enum;
using RPGHeroes.HeroClasses;

namespace RPGHeroes.Test
{
    public class HeroClassTest
    {
        [Fact]
        public void EquipWeapon_ValidWeapon_EquipsWeaponInWeaponSlot()
        {
            HeroClass warrior = new Warrior("Frank");
            Weapon weapon = new Weapon("Phantom Axe", 1, WeaponTypes.Axe, 1);
            Weapon expected = weapon;

            warrior.EquipWeapon(weapon);
            Weapon? actual = (Weapon?)warrior.Equipment[EquipmentSlots.Weapon];

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquipWeapon_InvalidWeaponType_ThrowsInvalidWeaponException()
        {
            HeroClass warrior = new Warrior("Frank");
            Weapon weapon = new Weapon("Phantom Bow", 1, WeaponTypes.Bow, 1);
            string expected = "Your class can't use this weapontype or your level is too low";

            Exception exception = Assert.Throws<InvalidWeaponException>(() => warrior.EquipWeapon(weapon));
            string actual = exception.Message;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquipWeapon_WeaponTooHighReqLvl_ThrowsInvalidWeaponException()
        {
            HeroClass warrior = new Warrior("Frank");
            Weapon weapon = new Weapon("Phantom Axe", 2, WeaponTypes.Axe, 1);
            string expected = "Your class can't use this weapontype or your level is too low";

            Exception exception = Assert.Throws<InvalidWeaponException>(() => warrior.EquipWeapon(weapon));
            string actual = exception.Message;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquipArmor_ValidWeapon_EquipsArmorInCorrectSlot()
        {
            HeroClass warrior = new Warrior("Frank");
            Armor armor = new Armor("Plate Chestplate", 1, EquipmentSlots.Body,
                ArmorTypes.Plate, new HeroAttributes(1, 1, 1));
            Armor expected = armor;

            warrior.EquipArmor(armor);
            Armor? actual = (Armor?)warrior.Equipment[EquipmentSlots.Body];

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquipArmor_InvalidArmorType_ThrowsInvalidArmorException()
        {
            HeroClass warrior = new Warrior("Frank");
            Armor armor = new Armor("Leather Chestplate", 1, EquipmentSlots.Body,
                ArmorTypes.Leather, new HeroAttributes(1, 1, 1));
            string expected = "Your class can't use this armortype or your level is too low";

            Exception exception = Assert.Throws<InvalidArmorException>(() => warrior.EquipArmor(armor));
            string actual = exception.Message;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquipArmor_InvalidArmorTooHighReqLvl_ThrowsInvalidArmorException()
        {
            HeroClass warrior = new Warrior("Frank");
            Armor armor = new Armor("Plate Chestplate", 2, EquipmentSlots.Body,
                ArmorTypes.Plate, new HeroAttributes(1, 1, 1));
            string expected = "Your class can't use this armortype or your level is too low";

            Exception exception = Assert.Throws<InvalidArmorException>(() => warrior.EquipArmor(armor));
            string actual = exception.Message;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TotalAttributes_NoArmor_CalculatesTotalAttributesCorrectly()
        {
            HeroClass warrior = new Warrior("Frank");
            HeroAttributes expected = new HeroAttributes(5, 2, 1);

            HeroAttributes actual = warrior.TotalAttributes();

            Assert.Equivalent(expected, actual);
        }

        [Fact]
        public void TotalAttributes_OneArmor_CalculatesTotalAttributesCorrectly()
        {
            HeroClass warrior = new Warrior("Frank");
            Armor armor = new Armor("Plate Chestplate", 1, EquipmentSlots.Body,
                ArmorTypes.Plate, new HeroAttributes(1, 1, 1));
            HeroAttributes expected = new HeroAttributes(6, 3, 2);

            warrior.EquipArmor(armor);
            HeroAttributes actual = warrior.TotalAttributes();

            Assert.Equivalent(expected, actual);
        }

        [Fact]
        public void TotalAttributes_TwoArmor_CalculatesTotalAttributesCorrectly()
        {
            HeroClass warrior = new Warrior("Frank");
            Armor chestArmor = new Armor("Plate Chestplate", 1, EquipmentSlots.Body,
                ArmorTypes.Plate, new HeroAttributes(1, 1, 1));
            Armor headArmor = new Armor("Plate Helmet", 1, EquipmentSlots.Head,
                ArmorTypes.Plate, new HeroAttributes(1, 1, 1));
            HeroAttributes expected = new HeroAttributes(7, 4, 3);

            warrior.EquipArmor(chestArmor);
            warrior.EquipArmor(headArmor);
            HeroAttributes actual = warrior.TotalAttributes();

            Assert.Equivalent(expected, actual);
        }

        [Fact]
        public void TotalAttributes_ReplacedArmor_CalculatesTotalAttributesCorrectly()
        {
            HeroClass warrior = new Warrior("Frank");
            Armor chestArmor = new Armor("Plate Chestplate", 1, EquipmentSlots.Body,
                ArmorTypes.Plate, new HeroAttributes(1, 1, 1));
            Armor replacingArmor = new Armor("Plated Chestplate of Plate", 1, EquipmentSlots.Body,
                ArmorTypes.Plate, new HeroAttributes(2, 2, 2));
            HeroAttributes expected = new HeroAttributes(7, 4, 3);

            warrior.EquipArmor(chestArmor);
            warrior.EquipArmor(replacingArmor);
            HeroAttributes actual = warrior.TotalAttributes();

            Assert.Equivalent(expected, actual);
        }

        [Fact]
        public void Display_DefaultWarrior_ReturnsDisplayStringWithCorrectData()
        {
            HeroClass warrior = new Warrior("Frank");
            string expected = "Name: Frank\r\nClass: Warrior\r\nLevel: 1\r\nTotal Strength: 5\r\nTotal Dexterity: 2\r\nTotal Intelligence: 1\r\nDamage: 1\r\n";

            string actual = warrior.Display();

            Assert.Equal(expected, actual);
        }
    }
}
