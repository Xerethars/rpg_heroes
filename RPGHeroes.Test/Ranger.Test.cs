﻿using RPGHeroes.HeroAddons;
using RPGHeroes.HeroClasses;

namespace RPGHeroes.Test
{
    public class RangerTest
    {
        [Fact]
        public void Constructor_HeroName_CreatesRangerWithCorrectName()
        {
            Ranger ranger = new Ranger("Frank");
            string expected = "Frank";

            string actual = ranger.Name;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Constructor_HeroName_CreatesRangerWithCorrectStartingLevel()
        {
            Ranger ranger = new Ranger("Frank");
            int expected = 1;

            int actual = ranger.Level;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Constructor_HeroName_CreatesRangerWithCorrectStartingAttributes()
        {
            Ranger ranger = new Ranger("Frank");
            int expectedStrength = 1;
            int expectedDexterity = 7;
            int expectedIntelligence = 1;

            Assert.Multiple(
                () => Assert.Equal(expectedStrength, ranger.LevelAttribute.Strength),
                () => Assert.Equal(expectedDexterity, ranger.LevelAttribute.Dexterity),
                () => Assert.Equal(expectedIntelligence, ranger.LevelAttribute.Intelligence)
            );
        }

        [Fact]
        public void LevelUp_SingleLevel_IncreasesLevelByOne()
        {
            Ranger ranger = new Ranger("Frank");
            int expected = 2;

            ranger.LevelUp();
            int actual = ranger.Level;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_SingleLevel_IncreaseAttributesByStr1Dex5Int1()
        {
            Ranger ranger = new Ranger("Frank");
            int expectedStrength = ranger.LevelAttribute.Strength + 1;
            int expectedDexterity = ranger.LevelAttribute.Dexterity + 5;
            int expectedIntelligence = ranger.LevelAttribute.Intelligence + 1;

            ranger.LevelUp();

            Assert.Multiple(
                () => Assert.Equal(expectedStrength, ranger.LevelAttribute.Strength),
                () => Assert.Equal(expectedDexterity, ranger.LevelAttribute.Dexterity),
                () => Assert.Equal(expectedIntelligence, ranger.LevelAttribute.Intelligence)
            );
        }

        [Fact]
        public void Damage_NoWeapon_ReturnsCorrectDamage()
        {
            Ranger ranger = new Ranger("Frank");
            int weaponDamage = 1;
            int scalingFactor = 1;
            int damageAttribute = ranger.TotalAttributes().Dexterity;
            double expected = weaponDamage * (scalingFactor + damageAttribute / 100);

            double actual = ranger.Damage();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Damage_EquippedWeapon_ReturnsCorrectDamage()
        {
            Ranger ranger = new Ranger("Frank");
            int weaponDamage = 2;
            int scalingFactor = 1;
            int damageAttribute = ranger.TotalAttributes().Dexterity;
            Weapon weapon = new Weapon("Gigachad Bow", 1,
                HeroAddons.Enum.WeaponTypes.Bow, weaponDamage);
            double expected = weapon.WeaponDamage * (scalingFactor + damageAttribute / 100);

            ranger.EquipWeapon(weapon);
            double actual = ranger.Damage();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Damage_ReplacedWeapon_ReturnsCorrectDamage()
        {
            Ranger ranger = new Ranger("Frank");
            int weaponDamage = 2;
            int scalingFactor = 1;
            int damageAttribute = ranger.TotalAttributes().Dexterity;

            Weapon weapon = new Weapon("Gigachad Bow", 1,
                HeroAddons.Enum.WeaponTypes.Bow, weaponDamage);
            Weapon replacingWeapon = new Weapon("Better Bow", 1,
                HeroAddons.Enum.WeaponTypes.Bow, weaponDamage * 2);

            double expected = replacingWeapon.WeaponDamage * (scalingFactor + damageAttribute / 100);

            ranger.EquipWeapon(weapon);
            ranger.EquipWeapon(replacingWeapon);
            double actual = ranger.Damage();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Damage_WeaponAndArmor_ReturnsCorrectDamage()
        {
            Ranger ranger = new Ranger("Frank");
            int weaponDamage = 1;
            int scalingFactor = 1;

            Weapon weapon = new Weapon("Gigachad Bow", 1,
                HeroAddons.Enum.WeaponTypes.Bow, weaponDamage);
            Armor armor = new Armor("Gigachad Leather", 1, HeroAddons.Enum.EquipmentSlots.Body,
                HeroAddons.Enum.ArmorTypes.Leather, new HeroAttributes(1, 1, 1));

            int damageAttribute = ranger.TotalAttributes().Dexterity + armor.ArmorAttributes.Dexterity;
            double expected = weapon.WeaponDamage * (scalingFactor + damageAttribute / 100);

            ranger.EquipWeapon(weapon);
            ranger.EquipArmor(armor);
            double actual = ranger.Damage();

            Assert.Equal(expected, actual);
        }
    }
}
