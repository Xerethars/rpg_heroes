﻿using RPGHeroes.HeroAddons;
using RPGHeroes.HeroAddons.Enum;
using RPGHeroes.HeroClasses;

namespace RPGHeroes.Test
{
    public class ArmorTest
    {
        [Fact]
        public void Constructor_ArmorParams_CreatesArmorWithCorrectName()
        {
            Armor armor = new Armor("Plate Chestplate", 1, EquipmentSlots.Body,
                ArmorTypes.Plate, new HeroAttributes(1, 1, 1));
            string expected = "Plate Chestplate";

            string actual = armor.Name;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Constructor_ArmorParams_CreatesArmorWithCorrectRquiredLevel()
        {
            Armor armor = new Armor("Plate Chestplate", 1, EquipmentSlots.Body,
                ArmorTypes.Plate, new HeroAttributes(1, 1, 1));
            int expected = 1;

            int actual = armor.RequiredLevel;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Constructor_ArmorParams_CreatesArmorWithCorrectSlot()
        {
            Armor armor = new Armor("Plate Chestplate", 1, EquipmentSlots.Body,
                ArmorTypes.Plate, new HeroAttributes(1, 1, 1));
            EquipmentSlots expected = EquipmentSlots.Body;

            EquipmentSlots actual = armor.Slot;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Constructor_ArmorParams_CreatesArmorWithCorrectArmorType()
        {
            Armor armor = new Armor("Plate Chestplate", 1, EquipmentSlots.Body,
                ArmorTypes.Plate, new HeroAttributes(1, 1, 1));
            ArmorTypes expected = ArmorTypes.Plate;

            ArmorTypes actual = armor.ArmorType;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Constructor_ArmorParams_CreatesArmorWithCorrectAttributes()
        {
            Armor armor = new Armor("Plate Chestplate", 1, EquipmentSlots.Body,
                ArmorTypes.Plate, new HeroAttributes(1, 1, 1));
            int expectedStrength = 1;
            int expectedDexterity = 1;
            int expectedIntelligence = 1;

            HeroAttributes actual = armor.ArmorAttributes;

            Assert.Multiple(
                () => Assert.Equal(expectedStrength, actual.Strength),
                () => Assert.Equal(expectedDexterity, actual.Dexterity),
                () => Assert.Equal(expectedIntelligence, actual.Intelligence)
            );
        }
    }
}
