﻿using RPGHeroes.HeroAddons;
using RPGHeroes.HeroClasses;

namespace RPGHeroes.Test
{
    public class RogueTest
    {
        [Fact]
        public void Constructor_HeroName_CreatesRogueWithCorrectName()
        {
            Rogue rogue = new Rogue("Frank");
            string expected = "Frank";

            string actual = rogue.Name;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Constructor_HeroName_CreatesRogueWithCorrectStartingLevel()
        {
            Rogue rogue = new Rogue("Frank");
            int expected = 1;

            int actual = rogue.Level;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Constructor_HeroName_CreatesRogueWithCorrectStartingAttributes()
        {
            Rogue rogue = new Rogue("Frank");
            int expectedStrength = 2;
            int expectedDexterity = 6;
            int expectedIntelligence = 1;

            Assert.Multiple(
                () => Assert.Equal(expectedStrength, rogue.LevelAttribute.Strength),
                () => Assert.Equal(expectedDexterity, rogue.LevelAttribute.Dexterity),
                () => Assert.Equal(expectedIntelligence, rogue.LevelAttribute.Intelligence)
            );
        }

        [Fact]
        public void LevelUp_SingleLevel_IncreasesLevelByOne()
        {
            Rogue rogue = new Rogue("Frank");
            int expected = 2;

            rogue.LevelUp();
            int actual = rogue.Level;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_SingleLevel_IncreaseAttributesByStr1Dex5Int1()
        {
            Rogue rogue = new Rogue("Frank");
            int expectedStrength = rogue.LevelAttribute.Strength + 1;
            int expectedDexterity = rogue.LevelAttribute.Dexterity + 4;
            int expectedIntelligence = rogue.LevelAttribute.Intelligence + 1;

            rogue.LevelUp();

            Assert.Multiple(
                () => Assert.Equal(expectedStrength, rogue.LevelAttribute.Strength),
                () => Assert.Equal(expectedDexterity, rogue.LevelAttribute.Dexterity),
                () => Assert.Equal(expectedIntelligence, rogue.LevelAttribute.Intelligence)
            );
        }

        [Fact]
        public void Damage_NoWeapon_ReturnsCorrectDamage()
        {
            Rogue rogue = new Rogue("Frank");
            int weaponDamage = 1;
            int scalingFactor = 1;
            int damageAttribute = rogue.TotalAttributes().Dexterity;
            double expected = weaponDamage * (scalingFactor + damageAttribute / 100);

            double actual = rogue.Damage();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Damage_EquippedWeapon_ReturnsCorrectDamage()
        {
            Rogue rogue = new Rogue("Frank");
            int weaponDamage = 2;
            int scalingFactor = 1;
            int damageAttribute = rogue.TotalAttributes().Dexterity;
            Weapon weapon = new Weapon("Gigachad Dagger", 1,
                HeroAddons.Enum.WeaponTypes.Dagger, weaponDamage);
            double expected = weapon.WeaponDamage * (scalingFactor + damageAttribute / 100);

            rogue.EquipWeapon(weapon);
            double actual = rogue.Damage();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Damage_ReplacedWeapon_ReturnsCorrectDamage()
        {
            Rogue rogue = new Rogue("Frank");
            int weaponDamage = 2;
            int scalingFactor = 1;
            int damageAttribute = rogue.TotalAttributes().Dexterity;

            Weapon weapon = new Weapon("Gigachad Dagger", 1,
                HeroAddons.Enum.WeaponTypes.Dagger, weaponDamage);
            Weapon replacingWeapon = new Weapon("Better Dagger", 1,
                HeroAddons.Enum.WeaponTypes.Dagger, weaponDamage * 2);

            double expected = replacingWeapon.WeaponDamage * (scalingFactor + damageAttribute / 100);

            rogue.EquipWeapon(weapon);
            rogue.EquipWeapon(replacingWeapon);
            double actual = rogue.Damage();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Damage_WeaponAndArmor_ReturnsCorrectDamage()
        {
            Rogue rogue = new Rogue("Frank");
            int weaponDamage = 1;
            int scalingFactor = 1;

            Weapon weapon = new Weapon("Gigachad Dagger", 1,
                HeroAddons.Enum.WeaponTypes.Dagger, weaponDamage);
            Armor armor = new Armor("Gigachad Leather", 1, HeroAddons.Enum.EquipmentSlots.Body,
                HeroAddons.Enum.ArmorTypes.Leather, new HeroAttributes(1, 1, 1));

            int damageAttribute = rogue.TotalAttributes().Dexterity + armor.ArmorAttributes.Dexterity;
            double expected = weapon.WeaponDamage * (scalingFactor + damageAttribute / 100);

            rogue.EquipWeapon(weapon);
            rogue.EquipArmor(armor);
            double actual = rogue.Damage();

            Assert.Equal(expected, actual);
        }
    }
}
